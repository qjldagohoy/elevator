# Elevator

Objective: Create a pseudo code, workflow diagram, and/or data model for an Elevator system

Rules :
-- A minimum of 3 elevators should exist in the system
-- A minimum of 8 floors
-- The direction of the elevator is up and down
-- A passenger can queue press the floor button to go down or up
-- A single elevator can cater multiple floor passenger queued